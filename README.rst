=======
customs
=======

Installing
==========

Use ``setup.py``::

   python setup.py build
   sudo python setup.py install


Testing
=======

To run the tests with the interpreter available as ``python``, use::

    make test

If you want to use a different interpreter, e.g. ``python3``, use::

    PYTHON=python3 make test
