#!/usr/bin/env python
# coding: utf-8


_string = lambda x: x.__name__ if isinstance(x, type) else x
_is_a = lambda x, y: isinstance(x, y)
_not_a = lambda x, y: not isinstance(x, y)


def type_of_object(models, object):
    '''Returns the type of the object if it conforms to any of the models.

    If the object does not conform to any of the models, no type will be
    returned. In this case, a path and message will be present indicating why
    the object failed to conform to any of the models.
    '''
    pass


def check(model, object):
    '''Checks whether the object being received (imported) conforms to the
    model type.

    If the object does not conform to the model type, a path and a message will
    be present indicating why the object failed to conform to the model.
    '''
    return _check(model, object)


def _check(model, target):
    '''Checks the target object against the model object.

    '''
    mtype = type(model) if _not_a(model, type) else model
    if not _valid_json_type(model):
        msg = 'bad model ("%s" is not a JSON type)' % mtype.__name__
        raise StandardError(msg)

    ttype = type(target)

    if _is_a(model, dict) or model == dict:
        return _visit_object(model, target)
    elif _is_a(model, list) or model == list:
        return _visit_array(model, target)
    elif mtype == int and ttype != int:
        return (False, None, 'not an integer')
    elif mtype == float and ttype != float:
        return (False, None, 'not a float')
    elif mtype == str and ttype != str:
        return (False, None, 'not a string')
    elif mtype == bool and ttype != bool:
        return (False, None, 'not a boolean')
    elif mtype is None and ttype is not None:
        return (False, None, 'not a null')
    return (True, None, None)


def _valid_json_type(json):
    json_type = json if _is_a(json, type) else type(json)
    if json_type == dict or  \
        json_type == list or  \
        json_type == int or   \
        json_type == float or \
        json_type == str or   \
        json_type == bool or  \
            json_type is None:
        return True
    return False


def _visit_object(model, target):
    '''Determines whether the target object conforms to the model object.

    For example:

        >>> _visit_object({"foo": str}, {"foo", "bar"})
        (True, None, None)
        >>> _visit_object({"foo": str}, {"foo", 42})
        (False, '["foo"]', 'not a string')

    '''
    if _not_a(target, dict):
        return (False, None, 'not an object')

    if _not_a(model, dict):
        if _not_a(model, type):
            raise AssertionError('not an object')
        # target is a type of dict, and so is the model
        return (True, None, None)

    # compare keys
    for k in model:
        if _not_a(k, str):
            msg = 'bad model ("%s" is not a valid key)' % type(k).__name__
            raise AssertionError(msg)
        if k not in target:
            msg = 'not found in target'
            return (False, '["%s"]' % k, msg)
        m_k = model[k]
        valid, path, msg = visit(m_k, target[k])
        if not valid:
            path = '' if not path else path
            path += '["%s"]' % k
            return (False, path, msg)
    return (True, None, None)


def _visit_array(model, target):
    '''Determines whether the target array conforms to the model array.

    For example:

        >>> _visit_array([int, int, str], [1, 2, "three"])
        (True, None, None)
        >>> _visit_object([int, int, str], [1, 2, 3])
        (False, '[2]', 'not a string')

    '''
    if _not_a(target, list):
        return (False, None, 'not an array')

    if _not_a(model, list):
        if _not_a(model, type):
            raise AssertionError()
        # target is a type of list, and so is the model
        return (True, None, None)

    # compare indices
    for x in xrange(0, len(model)):
        if not len(target) > x:
            msg = 'array item not found'
        valid, path, msg = visit(model[x], target[x])
        if not valid:
            path = '' if not path else path
            path += '[%d]' % x
            return (False, path, msg)
    return (True, None, None)
